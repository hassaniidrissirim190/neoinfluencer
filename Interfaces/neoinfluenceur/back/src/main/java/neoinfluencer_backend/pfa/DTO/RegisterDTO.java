package neoinfluencer_backend.pfa.DTO;

import java.util.Arrays;

public class RegisterDTO {
    private Long id;
    private String email;
    private  String firstname;
    private  String lastname;
    private  String company;
    private String address;
    private  String city;
    private String country;
    private Integer zipcode;
    private String password;
     public RegisterDTO() {
    }

    public RegisterDTO(String email, String firstname, String lastname, String company, String address, String city,
            String country, Integer zipcode,  String password) {
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.company = company;
        this.address = address;
        this.city = city;
        this.country = country;
        this.zipcode = zipcode;
        this.password = password;
    }

    public RegisterDTO(Long id, String email, String firstname, String lastname, String company, String address, String city, String country, Integer zipcode,
     String password) {
        this.id = id;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.company = company;
        this.address = address;
        this.city = city;
        this.country = country;
       
       
        this.zipcode = zipcode;
        this.password = password;

       
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getZipcode() {
        return zipcode;
    }

    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   

   
}
