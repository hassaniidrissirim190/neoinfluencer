package neoinfluencer_backend.pfa.controller;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.websocket.Session;
import neoinfluencer_backend.pfa.DTO.LoginDTO;
import neoinfluencer_backend.pfa.DTO.RegisterDTO;
import neoinfluencer_backend.pfa.payload.response.LoginResponse;
import neoinfluencer_backend.pfa.service.UserService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/auth")
public class UserController {

      // private final SessionRepository<? extends Session> sessionRepository;


    @Autowired
    private UserService userService;

     @PostMapping("/registration")
    public ResponseEntity<String>  register(@Validated @RequestBody RegisterDTO user) {
           String company = userService.register(user);
           if(company !=""){
            return ResponseEntity.ok().body("{\"status\": \"success\"}");
           }
           else if(company.equals("exist")){
             return ResponseEntity.ok().body("{\"status\": \"exist\"}");
           }
           return ResponseEntity.ok().body("{\"status\": \"failed\"}");
     }

    //   @PostMapping("/login")
    // public ResponseEntity<?> login(@Validated @RequestBody LoginDTO user) {
       
    //     LoginResponse loginMessage = userService.login(user);
    //     return ResponseEntity.ok(loginMessage);
    //  }
    
}
