package neoinfluencer_backend.pfa.service;

import neoinfluencer_backend.pfa.DTO.LoginDTO;
import neoinfluencer_backend.pfa.DTO.RegisterDTO;
import neoinfluencer_backend.pfa.model.User;
import neoinfluencer_backend.pfa.payload.response.LoginResponse;
import neoinfluencer_backend.pfa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

   private final PasswordEncoder passwordEncoder;

@Autowired
public UserService(PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
}

    public void updateAccessToken(long id, String accessToken) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("User not found with ID: " + id));
        user.setAccessToken(accessToken);
        userRepository.save(user);
    }

    public String getAccessToken(Long userId) {
        Optional<User> userOptional = userRepository.findById(userId);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            return user.getAccessToken();
        } else {
            return null; // or you can return a default value or handle the case accordingly
        }
    }

    //========================== REGISTRATION ==========================
    public String register(RegisterDTO registerDTO) {
    // Check if the email is already registered
    if (userRepository.existsByEmail(registerDTO.getEmail())) {
        throw new IllegalArgumentException("exist");
    }

    // Create a new user
    User user = new User(
            registerDTO.getId(),
            registerDTO.getEmail(),
            registerDTO.getFirstname(),
            registerDTO.getLastname(),
            registerDTO.getCompany(),
            registerDTO.getAddress(),
            registerDTO.getCity(),
            registerDTO.getCountry(),
            registerDTO.getZipcode(),
            this.passwordEncoder.encode(registerDTO.getPassword())
    );

    userRepository.save(user);
    String company = registerDTO.getCompany();
    return company;
}

    public LoginResponse login(LoginDTO loginDTO) {
        String msg = "";
        List<User> users = userRepository.findAllByEmail(loginDTO.getEmail());

        if (!users.isEmpty()) {
            String password = loginDTO.getPassword();
            Boolean isLoginSuccessful = false;
            Long id = (long) 1;

            for (User user : users) {
                String encodedPassword = user.getPassword();
                Boolean isPwdRight = passwordEncoder.matches(password, encodedPassword);
                if (isPwdRight) {
                    isLoginSuccessful = true;
                    id = user.getId();
                    break;
                }
            }

            if (isLoginSuccessful) {
                return new LoginResponse("Login Success", true, id);
            } else {
                return new LoginResponse("Login Failed", false);
            }
        } else {
            return new LoginResponse("Email doesn't exist", false);
        }
    }

    public UserDetails getUserDetailsByEmail(String email) {
        return null;
    }

  
}
