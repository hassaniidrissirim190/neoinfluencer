package neoinfluencer_backend.pfa.controller;

import neoinfluencer_backend.pfa.exception.ResourceNotFoundException;
import neoinfluencer_backend.pfa.model.User;
import neoinfluencer_backend.pfa.repository.UserRepository;
import neoinfluencer_backend.pfa.service.UserService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")

public class NeoController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;
    @GetMapping("/users")
    public List<User> getAllUsers(){

        System.out.println("jjfndnfkje");
        return userRepository.findAll();
    }

    // A modifier avec l 'ajout d'exception !!!
    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUserById(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("user not found for this id :: " + userId));
        return ResponseEntity.ok().body(user);
    }
    @PostMapping("users/Createusers")
    public User createUser(@Validated @RequestBody User user) {
           return userRepository.save(user);
     }



    @PutMapping("users/Updateusers/{id}")
    public ResponseEntity<User> updateUser(@PathVariable(value = "id") Long userId,
                                                   @Validated @RequestBody User userDetails) throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("user not found for this id :: " + userId));

        user.setAboutme(userDetails.getAboutme());
        user.setEmail(userDetails.getEmail());
        user.setCity(userDetails.getCity());
        user.setAddress(userDetails.getAddress());
        user.setCompany(userDetails.getCompany());
        user.setCountry(userDetails.getCountry());
        user.setFirstname(userDetails.getFirstname());
        user.setLastname(userDetails.getLastname());
        user.setUsername(userDetails.getUsername());
        user.setZipcode(userDetails.getZipcode());
        //user.setProfileimage(file.getBytes());
        // User updatedEmployee = UserRepository.save(user);
      //  UserRepository userRepository = obtainUserRepository(); // Obtain an instance of UserRepository, e.g., through dependency injection or creating a new instance
        User updateduSer = userRepository.save(user);
        return ResponseEntity.ok(updateduSer);


    }
    @PutMapping( "users/upload/image/{id}")
    @Transactional

    public ResponseEntity<User> uploadImage(@PathVariable(value = "id") Long userId,
                                            @RequestParam("image") MultipartFile image)
            throws IOException, ResourceNotFoundException {

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("user not found for this id :: " + userId));



            byte[] byteObjects = new byte[image.getBytes().length];

            int i = 0;

            for (byte b : image.getBytes()){
                byteObjects[i++] = b;
            }
        System.out.println(byteObjects);
            user.setProfileimage(byteObjects);


        User updateduSer = userRepository.save(user);
        return ResponseEntity.ok(updateduSer);

    }



    @DeleteMapping("/users/{id}")
    public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("user not found for this id :: " + userId));

        userRepository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @PutMapping("/users/{id}/setAccessToken")
    public void updateAccessToken(@PathVariable long id,@RequestBody String accessToken){
        userService.updateAccessToken(id,accessToken);
    }

    @GetMapping("/users/{userId}/getAccessToken")
    public String getAccessToken(@PathVariable Long userId) {
        return userService.getAccessToken(userId);
    }



}
