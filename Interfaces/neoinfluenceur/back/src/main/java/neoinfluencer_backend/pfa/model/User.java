package neoinfluencer_backend.pfa.model;

import jakarta.persistence.*;
import lombok.Builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Builder
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "username")

    private String username;
    @Column(name = "email")

    private String email;
    @Column(name = "firstname")

    private String firstname;
    @Column(name = "lastname")

    private String lastname;
    @Column(name = "company")

    private String company;
    @Column(name = "address")

    private String address;
    @Column(name = "city")

    private String city;
    @Column(name = "country")

    private String country;
    @Lob

   @Column(name = "profileimage", columnDefinition = "LONGVARBINARY")
     private byte[] profileimage;

    @Column(name = "aboutme")


    private String aboutme;
    @Column(name = "zipcode")


    private Integer zipcode;

    @Column(name="accessToken")
    private String accessToken;

     @Column(name="password", length = 255)
    private String password;

    public User(Long id, String email, String firstname, String lastname, String company, String address,
            String city, String country, Integer zipcode, String password) {
        this.id= id;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.company = company;
        this.address = address;
        this.city = city;
        this.country = country;
        this.zipcode = zipcode;
        this.password = password;
    }

    public User(Long id, String username, String email, String firstname, String lastname, String company,
            String address, String city, String country, byte[] profileimage, String aboutme, Integer zipcode,
            String accessToken, String password) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.company = company;
        this.address = address;
        this.city = city;
        this.country = country;
        this.profileimage = profileimage;
        this.aboutme = aboutme;
        this.zipcode = zipcode;
        this.accessToken = accessToken;
        this.password = password;
    }

    public User(Long id, String username, String email, String firstname, String lastname, String company, String address, String city, String country, byte[] profileimage, String aboutme, Integer zipcode,String accessToken) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.company = company;
        this.address = address;
        this.city = city;
        this.country = country;
        this.profileimage = profileimage;
        this.aboutme = aboutme;
        this.zipcode = zipcode;
        this.accessToken = accessToken;
    }

    public User() {

    }

    public User(Object o, String ghiz, String s, String qsTRINF, String qsTRINF1, String qsTRINF2, String qsTRINF3, String qsTRINF4, Object o1, String qsTRINF5, int i,String accessToken) {
    }


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public byte[] getProfileimage() {
        return profileimage;
    }

    public void setProfileimage(byte[] profileimage) {
        this.profileimage = profileimage;
    }

    public String getAboutme() {
        return aboutme;
    }

    public void setAboutme(String aboutme) {
        this.aboutme = aboutme;
    }

    public Integer getZipcode() {
        return zipcode;
    }

    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }

    public void setAccessToken(String accessToken){
        this.accessToken = accessToken;
    }
    public String getAccessToken(){
        return this.accessToken;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", company='" + company + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", profileimage=" + Arrays.toString(profileimage) +
                ", aboutme='" + aboutme + '\'' +
                ", zipcode=" + zipcode +
                '}';
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
