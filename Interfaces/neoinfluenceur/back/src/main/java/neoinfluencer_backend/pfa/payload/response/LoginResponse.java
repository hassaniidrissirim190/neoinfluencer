package neoinfluencer_backend.pfa.payload.response;

public class LoginResponse {
    
       String message;
        Boolean status;
        Long id;
    public Long getId() {
            return id;
        }
        public void setId(Long id) {
            this.id = id;
        }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Boolean getStatus() {
        return status;
    }
    public LoginResponse(String message, Boolean status, Long id) {
        this.message = message;
        this.status = status;
        this.id = id;
    }
    public void setStatus(Boolean status) {
        this.status = status;
    }
    public LoginResponse(String message, Boolean status) {
        this.message = message;
        this.status = status;
    }
}
