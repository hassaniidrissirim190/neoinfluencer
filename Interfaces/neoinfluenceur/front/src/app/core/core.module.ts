import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RegisterationComponent } from './registeration/registeration.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule} from '@angular/material/form-field';

import {MatIconModule} from '@angular/material/icon';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import { NavBarDashComponent } from './nav-bar-dash/nav-bar-dash.component';

@NgModule({
  declarations: [
    SidebarComponent,
    NavBarComponent,
    RegisterationComponent,
    HomeComponent,
    LoginComponent,
    NavComponent,
    NavBarDashComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,MatIconModule,
    FormsModule
  ],
  exports:[
    SidebarComponent,
    NavBarComponent,
    RegisterationComponent,
    NavComponent,
    NavBarDashComponent
  ], providers: [
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'outline'}}
  ]

})
export class CoreModule { }
