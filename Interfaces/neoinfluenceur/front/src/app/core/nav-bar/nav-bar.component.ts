import { AuthUserService } from 'src/app/shared/services/auth/auth-user.service';
import { UserService } from './../../shared/services/user.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent  implements OnInit{
  currentUser :any;
  id:any;
  profileImage="assets/auth/pd.png";
  
  
  constructor(private userService:UserService, private userAuth: AuthUserService,  private router:Router) {}
  ngOnInit(): void {

    this.id = this.userAuth.getId()
    this.getUser(this.id);
    
  }
  
@Output() menuClick = new EventEmitter<boolean>();
  opening:boolean = false;

  open(){

    this.menuClick.emit(!this.opening)
    this.opening = !this.opening
  }
  

  @Input()
  hide!:boolean
  
  getUser(id: string): void {
    this.userService.getbyId(id).subscribe(
      (data: any) => {
        this.currentUser = data;
        console.log(typeof (data.profileimage));

        const base64Image = data.profileimage;
        if (base64Image) {
          this.profileImage = 'data:image/*;base64,' + base64Image;
        }
        console.log(this.profileImage);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  toDashboard(){
    if(this.id!=null){
      console.log("in"+this.id)
      this.router.navigate(['/dashboard']);
    }else{
      this.router.navigate(['/registration']);
      console.log("out"+this.id)

    }
  }
}
