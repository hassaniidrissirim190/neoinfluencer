import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthUserService } from 'src/app/shared/services/auth/auth-user.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { ConfirmedValidator } from 'src/app/shared/services/auth/confirmed.validator';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registeration',
  templateUrl: './registeration.component.html',
  styleUrls: ['./registeration.component.css']
})
export class RegisterationComponent implements OnInit{


  registration:FormGroup = new FormGroup({
    company : new FormControl(''),
    email : new FormControl(''),
    
    firstname:new FormControl(''),
    lastname:new FormControl(''),
    number : new FormControl(''),
    address : new FormControl(''),
    city : new FormControl(''),
    country : new FormControl(''),
    zipcode : new FormControl(''),
    password : new FormControl(''),
    confirmPassword : new FormControl(''),

  })
  submitted = false;
  hide = true;
  isLoading = true;
  constructor(private formBuilder: FormBuilder, private authService:AuthService,  private router:Router, private userAuth: AuthUserService){

  }
  ngOnInit() {
    this.registration = this.formBuilder.group({
      company: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],

      email : ['',[Validators.required, Validators.email]],
      
      firstname:['',Validators.required],
      lastname:['',Validators.required],
      number : ['',[Validators.required, Validators.pattern("^((\\+212-?)|0)?[0-9]{10}$")]],
      address : ['',Validators.required],
      city : [''],
      country : [''],
      zipcode : [''],
      password : ['', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(40)
      ]],
      confirmPassword: ['', Validators.required],

    },
    {
      validator: ConfirmedValidator('password', 'confirmPassword')
    })
    
  }
  get f(){
    return this.registration.controls;
  }
  //============================ register ==============================
  register() {

    this.submitted = true;

    if (this.registration.invalid) {
      Swal.fire({
        title: 'Sign Up',
        text: 'Fill all the fields',
        icon: 'error',
      });
      return;
    }
    let generateId = Date.now() + Math.floor(Math.random() * 100000)
   console.log(this.registration.value)
   let user = {id:generateId,...this.registration.value}
   this.authService.register(user).subscribe(
    (res: HttpResponse<string>) => {
      console.log(res);
      this.userAuth.userId = generateId
      Swal.fire({
        title: 'Sign Up',
        text: 'You signed up successfully',
        icon: 'success',
      });
      this.onReset()
      this.router.navigate(['/dashboard']);
    },
    (error:HttpErrorResponse) => {
      if(error["error"]["message"]=="exist"){
        console.log(error["error"]["message"]);
      Swal.fire({
        title: 'Sign Up',
        text: 'Email already exist !',
        icon: 'error',
      });
      }
      else{console.log(error["error"]["message"]);
      Swal.fire({
        title: 'Sign Up',
        text: 'Sign Up Failed',
        icon: 'error',
      });}
    }
  );
  
}
//====================
onReset(): void {
 
  this.submitted = false;

  this.registration.reset();
}



}