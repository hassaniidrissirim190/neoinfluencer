import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthUserService } from 'src/app/shared/services/auth/auth-user.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit{
  id:any
  @Input()
  profileImage: any;
  constructor(private userAuth: AuthUserService, private router:Router){
    
  }
  ngOnInit(): void {

    this.id = this.userAuth.getId()

  }
 

  toDashboard(){

    if(this.id!=null){
      console.log("in"+this.id)
      this.router.navigate(['/dashboard']);
    }else{
      this.router.navigate(['/registration']);
      console.log("out"+this.id)

    }

  }
}
