import { Component, Input, OnInit } from '@angular/core';
import { SideBarData } from './sideBar_data';
import { AuthUserService } from 'src/app/shared/services/auth/auth-user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit{
collapsed = false;
SideData = SideBarData;
@Input()
open!: boolean;
constructor(private userAuth: AuthUserService,  private router:Router){}
ngOnInit(): void {
}
Active(){
  this.collapsed=true;
}
//======================= chose configuration button =============================
click :boolean = false;
choseConfig(){
  this.click = !this.click
}
//======================= open menu for mobiles button ==============================
 
//======================= close menu for mobiles button =============================
logout(){
  this.userAuth.logout();
  this.router.navigate(['/home'])
}
}
