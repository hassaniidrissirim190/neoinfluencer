import { AuthUserService } from 'src/app/shared/services/auth/auth-user.service';
import { UserService } from './../../shared/services/user.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { PhotoService } from 'src/app/shared/services/photo.service';

@Component({
  selector: 'app-nav-bar-dash',
  templateUrl: './nav-bar-dash.component.html',
  styleUrls: ['./nav-bar-dash.component.css']
})
export class NavBarDashComponent  implements OnInit{
  currentUser :any;
  id:any=1;
  profileImage="assets/auth/pd.png";

    
  constructor(private userService:UserService, private userAuth: AuthUserService,  private router:Router,   private photoService: PhotoService) {}
 
  
 ngOnInit(): void {

    this.id = this.userAuth.getId()
    this.getUser(this.id)
    this.photoService.profilePhoto$.subscribe(photoUrl => {
      this.profileImage = photoUrl;
    });
    
  }
  
@Output() menuClick = new EventEmitter<boolean>();
  opening:boolean = false;

  open(){

    this.menuClick.emit(!this.opening)
    this.opening = !this.opening
  }
  


  getUser(id: string): void {
    this.userService.getbyId(id).subscribe(
      (data: any) => {
        this.currentUser = data;
        console.log(typeof (data.profileimage));

        const base64Image = data.profileimage;
        if (base64Image) {
          this.profileImage = 'data:image/*;base64,' + base64Image;
        }
        console.log(this.profileImage);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

toDashboard(){
  if(this.id!=null){
    console.log("in"+this.id)
    this.router.navigate(['/dashboard']);
  }else{
    this.router.navigate(['/registration']);
    console.log("out"+this.id)

  }
}
}