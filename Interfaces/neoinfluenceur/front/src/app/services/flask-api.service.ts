import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class FlaskApiService {
  private apiUrl = 'http://localhost:3000';
  constructor(private http:HttpClient) { }

  predictSentiment(data: any[]): Observable<any> {
  return this.http.post<any>(`${this.apiUrl}/predict`, { data });
}
}
