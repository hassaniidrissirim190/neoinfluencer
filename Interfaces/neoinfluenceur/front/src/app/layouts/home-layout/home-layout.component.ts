import { Component, OnInit } from '@angular/core';
import { AuthUserService } from 'src/app/shared/services/auth/auth-user.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.css']
})
export class HomeLayoutComponent implements OnInit{
  currentUser :any;

  userId:any
  profile="assets/auth/pd.png"
  constructor(private userAuth: AuthUserService, private userService: UserService) {}
  ngOnInit(): void {
      this.userId = this.userAuth.getId()
      this.getUser(this.userId);
  }
  getUser(id: string): void {
    this.userService.getbyId(id).subscribe(
      (data: any) => {
        this.currentUser = data;
        console.log(typeof (data.profileimage));

        const base64Image = data.profileimage;
        if (base64Image) {
          this.profile = 'data:image/*;base64,' + base64Image;
        }
        console.log(this.profile);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
}


