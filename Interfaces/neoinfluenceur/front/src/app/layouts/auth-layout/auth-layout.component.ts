import { Component } from '@angular/core';

@Component({
  selector: 'app-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.css']
})
export class AuthLayoutComponent {

  openMenuVal!: boolean;

  openMenu(event :boolean){

    this.openMenuVal = event;
    console.log(this.openMenuVal);
  }
}
