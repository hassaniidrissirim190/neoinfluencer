import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup ,} from '@angular/forms';
import {MatRadioModule} from '@angular/material/radio';
import {  ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { CanalService } from 'src/app/shared/services/canal.service';
import { PostsService } from 'src/app/shared/services/posts.service';
import { YoutubeService } from 'src/app/shared/services/youtube.service';
import Swal from 'sweetalert2';
import { DomSanitizer,SafeResourceUrl  } from '@angular/platform-browser';
import { AuthUserService } from 'src/app/shared/services/auth/auth-user.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
//=========== FOR FB =================
  tokenForm!: FormGroup;

  selectedPosts: any= [];
  Data !: any;
  PageData!:any;
  nbPost!:any;
  selectAllChecked = false; // Add selectAllChecked property
  msg = "Start the analysing journey🚀!";
  img = "assets/Designs/acees.png"
//========== FOR YOUTUBE =================
  urlForm!: FormGroup;
  url:any
  idVideo=""
  videoDetails:any
  ytbComments:any
  videoUrl=""
  //=========== POP UP MSGS =====================
  isSuccess = false
  isLoading = true;
  //============= user id====================
  idUser:any;
  accessToken :any
  //============== chosen canal ===============
  canal=''
  constructor(private formBuilder: FormBuilder, private http: HttpClient, private postsService: PostsService,
    private router:Router,private activeRoute: ActivatedRoute,private canalService:CanalService,
    private youtubeService:YoutubeService,private sanitizer: DomSanitizer,private userAuth: AuthUserService
    ){
    
  }
  apiLoaded = false;


getSanitizedUrl(): SafeResourceUrl  {

    const sanitizedUrl = this.videoUrl.replace(/[^\w:]/gi, ''); // Replace any non-alphanumeric characters
    return this.sanitizer.bypassSecurityTrustResourceUrl(sanitizedUrl)
  
  
}

  ngOnInit(){
   
        //======== GET CANAL TO ANALYSE FROM THE ROUTER URL===
          this.idUser = this.userAuth.getId()
        this.canal = this.canalService.chosenCanal;
        console.log(this.canal)
        this.isLoading = false
        //======= in case user has not select any canal =====
        if(this.canal==undefined||this.canal==""){
          this.msg="Go back and select a canal !"
        }
      ///----- facebook -------------------
        this.tokenForm = this.formBuilder.group({
          token : new FormControl(''),
          startDate : new FormControl(''),
        })
      /// ------ youtube -----------------
      this.urlForm = this.formBuilder.group({
        url : new FormControl(''),
      })
      
     
      //====== POPULATE THE POSTS IF THE ACCESS TOKEN HAS BEEN ALREADY SET ===
      if(this.canal=='facebook'){
        this.isLoading=true
        this.checkAccessToken(this.idUser);
      }

  }

  //=================================== FUNCTIONS for FB========================
  //======= submit analyse with FB ======================
  submit(){

    console.log(this.tokenForm.value)
    this.accessToken = this.tokenForm.value.token;
    let start = this.tokenForm.value.startDate;
    if(this.accessToken!=""){
      this.setAccessToken(this.accessToken, this.idUser)
      this.getPosts(this.accessToken);
      this.tokenForm.reset()
    }else{
      Swal.fire({
        icon: 'error',
        title: 'Pay attention',
        text: '🚨Please enter a valid acces token',
      }); 
      return 
    }

  }
  //=========== SELECT POSTS / COLLECT COMMENTS =============================

  selectPost(post:any){
    let chose = {id:post.id,comments:post.comments,likes:post.likes}
    this.selectedPosts.push(chose)
    //------------- get comments -------------
    this.postsService.setComents(post?.comments)
    console.log(this.selectedPosts)
    console.log(this.postsService.comments)

  }
  //============ GO TO TrendingProject TO ANAYLSE SELETED COMMENTS  ==========
   //========================= ANAYLSE SELETED COMMENTS======================
   analyse(){
    if(this.selectedPosts?.length==0){
      Swal.fire({
        icon: 'error',
  title: 'Pay attention',
        text: 'Select at least one post',
      });    }else if(this.postsService.comments?.length==0){
        Swal.fire({
          icon: 'error',
          title: 'Pay attention',
          text: 'Select post that have comments ',
        });    }else{

      const navigationExtras: NavigationExtras = {
        state: {
          selectedPosts: this.selectedPosts,
          pageData:this.PageData,
          fbPosts:this.Data,
          nbPost:this.Data.length,
          canal:'facebook'
        }
      };
      this.router.navigate(['../FollowTrend'], navigationExtras);
    }
  
    }
//================ getPageData============================================
  // ...

  getPageData(accessToken: any) {
    this.postsService.getPageData(this.accessToken)
    .subscribe(
      (data) => {
        console.log('API Response:', data);
        const pageData = {
          name: data.name,
          fanCount: data.fan_count,
          pictureUrl: data.picture.data.url,
        };
              console.log('Page data:', pageData);
              this.PageData = pageData;
              console.log('this is page Data',this.PageData);

              return pageData;
            },error=>{
              this.msg = error.status
            }
            );
            //console.log("Facebook Data: ", this.PageData);
         
        
       
      
  }
    //=========================  SELECT POSTS =============================
 
  
    togglePostSelection(post: any) {
      const typedPost = post ;
      const isPostSelected = this.selectedPosts.some((selectedPost: { id: any; }) => selectedPost.id === typedPost.id);
    
      if (isPostSelected) {
        const index = this.selectedPosts.findIndex((selectedPost: { id: any; }) => selectedPost.id === typedPost.id);
        if (index !== -1) {
          this.selectedPosts.splice(index, 1);
          console.log('Post deselected:', typedPost);
          console.log('posts',this.selectedPosts);
  
        }
      } else {
        if (this.selectedPosts.length >= 2) {
          Swal.fire({
            title: 'Information about Selection ',
            text: ' If you select 2 posts, we will compare them. If you select only 1 post, we will analyze it individually. Finally select extra posts if you want a general analysis.',
            icon: 'warning',
            confirmButtonText: 'OK'
          });
        }
        const selectedPost: any = {
          id: typedPost.id,
          comments: typedPost.comments,
          likes: typedPost.likes
        
        };
    
        this.selectedPosts.push(selectedPost);
        console.log('Selected posts:', this.selectedPosts);
    
        this.postsService.setComents(typedPost.comments);
  
  
      }
    }
  
      //========================== GET POSTS ============================================================
  getPosts(accessToken:any){
    this.postsService.getPostFromGraphApi(this.accessToken)
     .subscribe((data) => {
      this.Data = data.posts?.data.map((post: {id:any,likes:any, comments: any; created_time: any; full_picture: any; shares:any; message: any; }) => {
         let postData = {
           id:post?.id,
           title: post?.message,
           img: post?.full_picture,
           shares: post?.shares,
           date: post.created_time,
           comments: post.comments,
           likes:post?.likes
         };
         console.log('this is post dat',postData)
         return postData
       });
       console.log("facebook posts====>", this.Data);
       console.log("djj hdo chaal",this.Data.length);
       this.isSuccess = true;
       this.isLoading = false
     },
     (error) => {
       if (error.status == 400) {
         this.msg = "Your Access Token is incorrect";
         this.img = "assets/Status/400.png";
       }
       console.log(error);
     });
   
   }
    // ============= check if the post is checked or not =================
    isPostSelected(post: any): boolean {
      return this.selectedPosts.some((selectedPost: { id: any; }) => selectedPost.id === post.id);
    }
   //================= Select all posts =================*
   selectAllPosts() {
    // Clear the selectedPosts array
    this.selectedPosts = [];
    // Add all posts to the selectedPosts array
    for (const post of this.Data) {
      const selectedPost: any = {
        id: post.id,
        comments: post.comments,
        likes: post.likes,
      };
  
      this.selectedPosts.push(selectedPost);
    }
    if (this.selectedPosts.length >= 2) {
      Swal.fire({
        title: 'Information about Selection ',
        text: ' If you select 2 posts, we will compare them. If you select only 1 post, we will analyze it individually. Finally select extra posts if you want a general analysis.',
        icon: 'warning',
        confirmButtonText: 'OK'
      });
    }
  
    console.log('Selected all  posts:', this.selectedPosts);
  }
    //========================== CHECK THE ACCESS TOKEN ====================
    checkAccessToken(id:any) {
      //------- CHECK IFTHE USER HAS ALREADY AN ACCESS TOKEN ------
      this.postsService.getAccessToken(this.idUser)
      .subscribe((resultData: any)=>
      {
        if(resultData!=null){
           this.accessToken = resultData;
           this.getPosts(this.accessToken);
           this.getPageData(this.accessToken);
           this.isSuccess = true
           this.isLoading = false

        }else{
         //-------- user has no access token------
        }
      },(error: any)=>{
    if(error.status==0){
      this.msg = "Try later 😔";
      return
      }
        this.msg = error.message;
        console.log(error)
      }
      )
    }
  //=========================== SET OR UPDATE THE ACCES TOKEN =========================
  setAccessToken(accessToken:any,id:any){
    //---- send the access token ---------
    this.getPageData(accessToken);

    this.postsService.setAccessToken(this.accessToken,this.idUser).subscribe(res =>{
     console.log(res)
     this.isLoading = false

   },error=>{
     this.msg = error.message
   })
   }



  //======== FUNTIONS FOR YOUTUBE  ==================
  submitYT(){
        this.isLoading=true

    console.log(this.urlForm.value)
    this.url = this.urlForm.value.url;

    if(this.url!=""){
      this.youtubeService?.getVideoByUrl(this.url).subscribe(
        res=>{
          console.log(res)
          this.idVideo = res.videoDetails.id
          this.videoDetails = res.videoDetails
          this.ytbComments = res.comments
          this.urlForm.reset()
          // Inside your component code
        this.videoUrl = "https://www.youtube.com/watch?v="+this.idVideo; // The resource URL that needs to be sanitized
        this.isLoading = false
          
      const navigationExtras: NavigationExtras = {
        state: {
          youtubeComments: this.ytbComments,
          videoDetails:this.videoDetails,
          videoUrl:this.videoUrl,
          idVideo:this.idVideo,
          canal:'youtube'
        }
      };
      this.router.navigate(['../FollowTrend'], navigationExtras);
     },error=>{
      this.urlForm.reset()

          console.log(error)
          this.msg = "Error "+error.status
          if(error.status==0){
            Swal.fire({
              title: 'Information about URL ',
              text: 'Please enter valid url ',
              icon: 'warning',
              confirmButtonText: 'OK'
            });
          }
        }
      )
    }else{
      Swal.fire({
        title: 'Information about URL ',
        text: 'Please enter valid url ',
        icon: 'warning',
        confirmButtonText: 'OK'
      });
      
    }
  }
}
