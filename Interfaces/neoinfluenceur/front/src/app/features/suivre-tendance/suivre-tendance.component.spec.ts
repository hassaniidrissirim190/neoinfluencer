import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuivreTendanceComponent } from './suivre-tendance.component';

describe('SuivreTendanceComponent', () => {
  let component: SuivreTendanceComponent;
  let fixture: ComponentFixture<SuivreTendanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuivreTendanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SuivreTendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
