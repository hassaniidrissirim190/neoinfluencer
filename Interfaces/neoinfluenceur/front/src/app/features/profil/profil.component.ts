import { UserService } from './../../shared/services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from 'src/app/shared/models/client.model';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import Swal from 'sweetalert2';
import { AuthUserService } from 'src/app/shared/services/auth/auth-user.service';
import { PhotoService } from 'src/app/shared/services/photo.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  currentUser: Client = {
    id: '',
    username: '',
    firstname: '',
    lastname: '',
    email: '',
    company: '',
    address: '',
    city: '',
    country: '',
    aboutme: '',
    zipcode: '',
    profileimage: 'data:image/jpeg;base64,/9j/4QC8RXhpZgAASUkqAAgAAAAGABIBAwABAAAAAQAAABoBBQABAAAAVgAAABsBBQABAAAAXgAAACgBAwABAAAAAgAAABMCAwABAAAAAQAAAGmHBAABAAAAZgAAAAAAAABIAAAAAQAAAEgAAAABAAAABgAAkAcABAAAADAyMTABkQcABAAAAAECAwAAoAcABAAAADAxMDABoAMAAQAAAP//AAACoAQAAQAAANwFAAADoAQAAQAAAOgDAAAAAAAA/9sAQwAFAwQEBAMFBAQEBQUFBgcMCAcHBwcPCwsJDBEPEhIRDxERExYcFxMUGhURERghGBodHR8fHxMXIiQi',
  };

  message = '';
  id: any;
  profileImage = "assets/auth/pd.png";
  selectedImageFile: File | undefined;
  file: any;
  uploadedImage: any;


  constructor(
    public sanitizer: DomSanitizer,
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private userAuth: AuthUserService,
    private photoService: PhotoService) { }

  ngOnInit(): void {
    this.message = '';
    this.id = this.userAuth.userId;
    this.getUser(this.id);
  }


  getUser(id: string): void {
    this.userService.getbyId(id).subscribe(
      (data: any) => {
        this.currentUser = data;
        console.log(typeof (data.profileimage));

        const base64Image = data.profileimage;
        if (base64Image) {
          this.profileImage = 'data:image/*;base64,' + base64Image;
        }
        console.log(this.profileImage);
      },
      (error: any) => {
        console.log(error);
      }
    );
  }


  handleImageUpload(event: any) {
    this.uploadedImage = event.target.files[0];

    // Optionally, you can display a preview of the selected image
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.profileImage = e.target.result;
    };
    reader.readAsDataURL(this.uploadedImage);
    console.log('this is handle image upload after click', this.uploadedImage);
  }

  imageUploadAction(): void {
    if (!this.uploadedImage) {
      console.log('No image selected.');
      return;
    }

    const imageFormData = new FormData();
    imageFormData.append('image', this.uploadedImage, this.uploadedImage.name);

    console.log("Before appending image:", imageFormData);
    console.log("Uploaded image name:", this.uploadedImage.name);

    this.userService.updateUserImage(this.currentUser.id, imageFormData)
      .subscribe(
        response => {
          console.log(response);
          const message = response.message ? response.message : 'Your information was updated successfully!';
          console.log(message);
          Swal.fire(message);
        },
        error => {
          console.log(error);
        }
      );
  }

  updateProfile(): void {
    this.message = '';

    this.userService.updateUser(this.currentUser.id, this.currentUser)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message ? response.message : 'Your information was updated successfully!';
          console.log(this.message);
          Swal.fire(this.message);
          this.photoService.updateProfilePhoto(this.profileImage);
        },
        error => {
          console.log(error);
        });

    // Update the image only if a new image file is selected
    if (this.uploadedImage) {
      this.imageUploadAction();
    }
  }
}
