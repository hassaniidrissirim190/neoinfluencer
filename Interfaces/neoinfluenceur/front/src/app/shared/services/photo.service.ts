import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor() { }
  private profilePhotoSubject = new BehaviorSubject<string>('assets/auth/pd.png');
  profilePhoto$ = this.profilePhotoSubject.asObservable();

  updateProfilePhoto(photoUrl: string) {
    this.profilePhotoSubject.next(photoUrl);
  }
}
