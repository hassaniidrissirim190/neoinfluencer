import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LocalStorageService } from './storage/local-storage.service';

const DbEndpoint = 'http://localhost:8080/api/v1/users';
const FbEndepoint = 'https://graph.facebook.com/me?fields=posts{id,full_picture,shares,message,created_time,comments,likes},likes';
const FbEndepointData ='https://graph.facebook.com/me?fields=name,picture,fan_count,likes'

@Injectable({
  providedIn: 'root'
})

export class PostsService {

 comments:string[] = []
  constructor(private http: HttpClient) { }
  private getHeaders(): HttpHeaders {
    const token = LocalStorageService.getToken();
    return new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
  }
   // Replace with the actual authentication token

// Set the request headers with the authentication token

   getPageData(accessToken:any) :Observable<any>{
    const headers = this.getHeaders();
     return  this.http.get<any>(`${FbEndepointData}&access_token=${accessToken}`, { headers })
  }
  

  getPostFromGraphApi(accessToken: any):Observable<any> {
    const headers = this.getHeaders();
    return  this.http.get<any>(`${FbEndepoint}&access_token=${accessToken}`, { headers })
    
  }
  //================ get access token ==============
  getAccessToken(id:any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.get(`${DbEndpoint}/${id}/getAccessToken`, { headers, responseType: 'text' })

  }
  //================ set or update the acces token========================
  setAccessToken(accessToken:any,id:any):Observable<any> {
    const headers = this.getHeaders();
    return this.http.put(`${DbEndpoint}/${id}/setAccessToken`,accessToken,  { headers, responseType: 'text' })
  }
  //============ set comments ===================
  setComents(comments:any){
    const headers = this.getHeaders();
    this.comments.push(comments)
  }
}
