import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map, mergeMap, pluck, toArray } from 'rxjs/operators';
import { Observable, forkJoin } from 'rxjs';
import { LocalStorageService } from './storage/local-storage.service';


const baseUrl = 'http://localhost:8080/api/v1/users';
const token = LocalStorageService.getToken();
const headers = new HttpHeaders({
  'Authorization': `Bearer ${token}`
});
@Injectable({
  providedIn: 'root'
})
export class YoutubeService {
  apiKey: string = 'AIzaSyCikybeni2Jd8FdDlDgqqpn_fnBq1RxCiU';

  constructor(private http: HttpClient) {}

  getVideoByUrl(videoUrl: string): Observable<any> {
    const videoId = this.extractVideoId(videoUrl);
    
    const videoDetailsApiUrl = 'https://www.googleapis.com/youtube/v3/videos';
    const videoDetailsParams = new HttpParams()
      .set('key', this.apiKey)
      .set('id', videoId)
      .set('part', 'snippet,statistics');

      // const ratingDetailsApiUrl = 'https://www.googleapis.com/youtube/v3/videos/rate';
      // const ratingDetailsParams = new HttpParams()
      //   .set('id', videoId)
      //   .set('rating', 'like,dislike')
      //   ;

    const commentsApiUrl = 'https://www.googleapis.com/youtube/v3/commentThreads';
    const commentsParams = new HttpParams()
      .set('key', this.apiKey)
      .set('videoId', videoId)
      .set('part', 'snippet')
      .set('maxResults', '200');

    // const ratingDetailsRequest = this.http.get<any>(ratingDetailsApiUrl, { params: ratingDetailsParams });
    const videoDetailsRequest = this.http.get<any>(videoDetailsApiUrl, { params: videoDetailsParams });
    const commentsRequest = this.http.get<any>(commentsApiUrl, { params: commentsParams }).pipe(
      map((response: any) => response.items),
      mergeMap((comments: any[]) => comments),
      map((comment: any) => comment.snippet.topLevelComment.snippet.textOriginal),
      toArray()
    );

    return forkJoin([videoDetailsRequest, commentsRequest]).pipe(
      map(([videoDetailsResponse, commentsResponse]) => {
        return {
          videoDetails: videoDetailsResponse.items[0],
          comments: commentsResponse,       
        
        };
      })
    );
  }

  private extractVideoId(videoUrl: string): string {
    const regex = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?.*v=([a-zA-Z0-9_-]{11}).*/;
    const match = videoUrl.match(regex);
    if (match && match.length === 2) {
      return match[1];
    }
    return 'Invalid YouTube video URL';
  }
}
