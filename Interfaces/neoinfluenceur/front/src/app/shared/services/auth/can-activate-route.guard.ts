import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthUserService } from './auth-user.service';
import { LocalStorageService } from '../storage/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class CanActivateRouteGuard implements CanActivate {
  constructor(private auth: AuthUserService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // Check if the user is authenticated by checking if the user ID has a value
    if (LocalStorageService.hasToken() && LocalStorageService.isUserLoggedIn()) { 
      console.log("yes")
      return true; 
    } else {
      this.router.navigate(['/login']); 
      console.log("no")

      return false; 
    }
  }
  
}
