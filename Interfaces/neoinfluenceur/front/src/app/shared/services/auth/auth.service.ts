import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, tap } from 'rxjs';
import { LocalStorageService } from '../storage/local-storage.service';

export const AUTH_HEADER = "authorization";

const registerEndepoint = 'http://localhost:8080/api/auth/registration';
const loginEndepoint = 'http://localhost:8080/api/authentication/login';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private http: HttpClient,  private storageService: LocalStorageService) { }

  register(user:any) :Observable<any | string>{
    return  this.http.post<any>(`${registerEndepoint}`,user);

 }


login(username: string, password: string): any {
  console.log(username, password)
  return this.http.post<[]>(`${loginEndepoint}`,
    { username, password },
    { observe: 'response' })
    .pipe(
      tap(_ => this.log("User Authentication")),
      map((res: HttpResponse<any>) => {
        this.storageService.saveUser(res.body);
        this.storageService.saveUserId(res.body.userId);
        this.storageService.saveUserRole(res.body.role);
        const tokenLength = res.headers.get(AUTH_HEADER)?.length;
        const bearerToken = res.headers.get(AUTH_HEADER)?.substring(7, tokenLength);
        this.storageService.saveToken(bearerToken);
        return res;
      })
    )
}
//============================ 
log(message: string): void {
  console.log(`User Auth Service: ${message}`)
}
}
