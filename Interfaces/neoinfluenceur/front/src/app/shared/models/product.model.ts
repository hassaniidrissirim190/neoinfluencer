export class Product{
    public constructor(public idProduct:number,
                       public productName:string,
                       public description:string,
                       public image:any){}
}