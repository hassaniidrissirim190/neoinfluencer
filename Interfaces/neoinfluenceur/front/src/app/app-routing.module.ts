import { ProfilComponent } from './features/profil/profil.component';
import { SuivreTendanceComponent } from './features/suivre-tendance/suivre-tendance.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CanauxComponent } from './features/canaux/canaux.component';

import { PostsComponent } from './features/posts/posts.component';
import { RegisterationComponent } from './core/registeration/registeration.component';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import { HomeComponent } from './core/home/home.component';
import { LoginComponent } from './core/login/login.component';
import { CanActivateRouteGuard } from './shared/services/auth/can-activate-route.guard';

const routes: Routes = [                                                                                                      
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      { path: 'registration', component: RegisterationComponent },
      { path: 'login', component: LoginComponent }
    ]
  },
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      { path: 'home', component: HomeComponent }
    ]
  },
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      { path: '', component: DashboardComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'TrendingProject', component: PostsComponent },
      { path: 'configuration', component: CanauxComponent },
      { path: 'FollowTrend', component: SuivreTendanceComponent },
      { path: 'profil', component: ProfilComponent }
    ],
    canActivate: [CanActivateRouteGuard]
  },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
